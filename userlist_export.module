<?php

/**
 * Implements hook_permission().
 */
function userlist_export_permission() {
  return array(
    'administer userlist export' => array(
      'title' => t('administer userlist export'),
      'description' => t('TODO Add a description for \'administer userlist export\''),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function userlist_export_menu() {
  $items['admin/user/userlist_export'] = array(
    'title' => t('Userlist export'),
    'description' => 'Configure userlist export settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('userlist_export_settings_samba'),
    'access arguments' => array('administer userlist export'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/user/userlist_export/samba'] = array(
    'title' => t('Samba'),
    'description' => 'Configure userlist export settings for Samba',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('userlist_export_settings_samba'),
    'access arguments' => array('administer userlist export'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/user/userlist_export/svn'] = array(
    'title' => t('SVN'),
    'description' => 'Configure userlist export settings for SVN',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('userlist_export_settings_svn'),
    'access arguments' => array('administer userlist export'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/user/userlist_export/export_samba'] = array(
    'title' => t('Export Samba'),
    'description' => 'Export userlist',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('userlist_export_samba'),
    'access arguments' => array('administer userlist export'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  $items['admin/user/userlist_export/export_svn'] = array(
    'title' => t('Export SVN'),
    'description' => 'Export SVN',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('userlist_export_svn'),
    'access arguments' => array('administer userlist export'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  return $items;
}

/**
 * Settings form
 */
function userlist_export_settings_samba($form, &$form_state) {
  $form['userlist_export_samba_file_prepend'] = array(
    '#type' => 'textfield',
    '#title' => t('File prepend text'),
    '#default_value' => variable_get('userlist_export_samba_file_prepend', 'write list = '),
    '#size' => 30,
    '#description' => t('Text to prepend to output file.'),
  );
  $form['userlist_export_samba_role'] = array(
    '#type' => 'radios',
    '#title' => t('Export users with this role'),
    '#default_value' => variable_get('userlist_export_samba_role', 3),
    '#options' => user_roles(TRUE),
  );
  $form['userlist_export_samba_username_prepend'] = array(
    '#type' => 'textfield',
    '#title' => t('Username prepend text'),
    '#default_value' => variable_get('userlist_export_samba_username_prepend', 'ADDOMAIN\\'),
    '#size' => 30,
    '#description' => t('Text to prepend to usernames.'),
  );
  $form['userlist_export_samba_filepath'] = array(
    '#type' => 'textfield',
    '#title' => t('Filename and path'),
    '#default_value' => variable_get('userlist_export_samba_filepath', '/samba/write_list.conf'),
    '#size' => 30,
    '#description' => t('Filename and path to write to when a user role is updated. Leave blank to not save a file on user role change.'),
  );
  return system_settings_form($form);
}

/**
 * Settings form
 */
function userlist_export_settings_svn($form, &$form_state) {
  $form['userlist_export_svn_role'] = array(
    '#type' => 'radios',
    '#title' => t('Export admin users with this role (these users will have rw access to entire SVN repository)'),
    '#default_value' => variable_get('userlist_export_svn_role', 3),
    '#options' => user_roles(TRUE),
  );
  $form['userlist_export_svn_access_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('SVN directory where access file resides (only users listed below will have access)'),
    '#default_value' => variable_get('userlist_export_svn_access_dir', 'config'),
    '#size' => 30,
  );
  $form['userlist_export_svn_access_role'] = array(
    '#type' => 'radios',
    '#title' => t('Users with access to access file directory'),
    '#default_value' => variable_get('userlist_export_svn_access_role', 3),
    '#options' => user_roles(TRUE),
  );
  return system_settings_form($form);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function userlist_export_samba($form, &$form_state) {
  $form['userlist_export_samba'] = array(
    '#type' => 'textarea',
    '#title' => t('Output'),
    '#default_value' => userlist_export_generate_samba(),
  );
  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function userlist_export_svn($form, &$form_state) {
  $form['userlist_export_svn'] = array(
    '#type' => 'textarea',
    '#title' => t('Output'),
    '#default_value' => userlist_export_generate_svn(),
  );
  return $form;
}

/**
 * Implements hook_user_update().
 */
function userlist_export_user_update(&$edit, $account, $category) {
  $path = variable_get('userlist_export_samba_filepath', '/samba/write_list.conf');
  if ($path != '') {
    $output = userlist_export_generate_samba();
    $file = file_directory_path() . $path;
    $fh = fopen($file, 'w');
    fwrite($fh, $output);
    fclose($fh);
  }
}

function userlist_export_generate_samba() {
  $output = variable_get('userlist_export_samba_file_prepend', 'write list = ');
  $prepend = variable_get('userlist_export_samba_username_prepend', 'ADDOMAIN\\');
  $user_list = userlist_export_build_list(variable_get('userlist_export_samba_role', 3));
  $output .= $user_list;
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function userlist_export_generate_svn() {
  $output = "[groups]\n";
  $access_dir = variable_get('userlist_export_svn_access_dir', 'config');
  $admin_list = userlist_export_build_list(variable_get('userlist_export_svn_role', 3));
  $access_list = userlist_export_build_list(variable_get('userlist_export_svn_access_role', 3));
  $output .= "admins = $admin_list\n";
  $output .= "uberadmins = $access_list\n";
  $groups = db_query("SELECT * FROM {og} INNER JOIN {purl} ON og.nid = purl.id ORDER BY purl.value");
  $group_array = array();
  while ($group = db_fetch_object($groups)) {
    $user_array = array();
    $users = db_query("SELECT * FROM {og_uid} INNER JOIN {users} ON og_uid.uid = users.uid WHERE nid = :nid", array(':nid' => $group->nid));
    while ($user = db_fetch_object($users)) {
      if (preg_match('/^[A-Za-z0-9]*$/', $user->name)) {
        $user_array[] = $user->name;
      }
    }
    $user_list = implode(',', $user_array);
    if (preg_match('/^[A-Za-z0-9-_]*$/', $group->value) && $group->value != 'admins' && $group->value != 'uberadmins' && $group->value != $access_dir && $user_list != '') {
      $group_array[] = $group->value;
      $output .= 'team_' . $group->value . ' = ' . $user_list . "\n";
    }
  }
  $output .= "\n[/]\n" . "@admins = rw\n" . "* = \n";
  foreach ($group_array as $group) {
    $output .= "[/$group]\n" . "@team_$group = rw\n";
  }
  $output .= "[/$access_dir]\n";
  $output .= "@uberadmins = rw\n";
  $output .= "* = \n";
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function userlist_export_build_list($rid) {
  $users = db_query("SELECT users.name FROM {users} INNER JOIN {users_roles} ON users.uid = users_roles.uid WHERE users_roles.rid = :users_roles.rid", array(':users_roles.rid' => $rid));
  while ($user = db_fetch_object($users)) {
    $user_array[] = $prepend . $user->name;
  }
  return implode(',', $user_array);
}
